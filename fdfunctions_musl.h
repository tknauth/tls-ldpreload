
/*

musl modifications

we will modify musl such that all libc functions define two symbols:
- a weak alias that conforms to the POSIX standard, e.g., read
- a strong name that can be called even if we would replace the weak symbol, e.g.,  __read   

example:
    int __socket(int domain, int type, int protocol) { ...  }
    void socket(int domain, int type, int protocol) __attribute__ ((weak, alias ("__socket")));
*/


#ifndef __FDFUNCTIONS_MUSL_H_
#define __FDFUNCTIONS_MUSL_H_

#include <sys/socket.h>
#include <errno.h>
#include <stdlib.h>

/*
   all fd-related functions that could be wrapped 
   (this header file contains the strong symbols)
   
*/

ssize_t _recv(int fd, void *buf, size_t len, int flags);
ssize_t _send(int fd, const void *buf, size_t len, int flags);
int _accept(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len);
int _accept4(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len, int flags);
int _bind(int sockfd, const struct sockaddr* addr, socklen_t addrlen);
int _connect(int socket, const struct sockaddr* address, socklen_t address_len);
ssize_t _read(int, void *, size_t);
ssize_t _write(int, const void *, size_t);
ssize_t _pread(int, void *, size_t, off_t);
ssize_t _pwrite(int, const void *, size_t, off_t);
ssize_t _sendmsg(int socket, const struct msghdr *message, int flags);
ssize_t _sendto(int socket, const void *buffer, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
ssize_t _recvfrom(int socket, void *restrict buffer, size_t length, int flags, struct sockaddr *restrict address, socklen_t *restrict address_len);
ssize_t _recvmsg(int socket, struct msghdr *message, int flags);
int _socket(int domain, int type, int protocol);
int _close(int fildes);
size_t ___stdio_read(FILE *f, unsigned char *buf, size_t len);
size_t ___stdio_write(FILE *f, unsigned char *buf, size_t len);
ssize_t _writev(int fd, const struct iovec *iov, int iovcnt);

#endif
