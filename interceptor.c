/*
    libc interceptor:
    
    objective: if no interceptor activated (during compilation), we have no overhead
    
    if interceptor is activated: we introduce some overhead since we wrap some libc functions

*/

#ifdef TLS_INTERCEPTOR

#include <sys/socket.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "fdfunctions_musl.h"
#include "interceptor.h"

#ifndef MAX_FDS_SUPPORTED
#define MAX_FDS_SUPPORTED 1024
#endif




// in case we are out of bound - we call some implementation that aborts the program

ssize_t __write_abort(void* context, int fildes, const void *buf, size_t nbyte) { 
    fprintf(stderr, "_write_abort: write not implemented\n"); 
    abort(); 
    return -1; 
}

int __accept_abort(void* context, int socket, struct sockaddr *restrict address, socklen_t *restrict address_len) {
    fprintf(stderr, "__accept_abort: accept not implemented\n"); 
    abort(); 
    return -1; 
}

ssize_t __read_abort(void* context, int a1, void *a2, size_t a3) { 
    fprintf(stderr, "_read_abort: read not implemented\n"); 
    abort(); 
    return -1; 
}

ssize_t __pread_abort(void* context, int a1, void *a2, size_t a3, off_t a4) { 
    fprintf(stderr, "_write_abort: write not implemented\n"); 
    abort(); 
    return -1; 
}

ssize_t __pwrite_abort(void* context, int a1, const void *a2, size_t a3, off_t a4) { 
    fprintf(stderr, "_pwrite_abort: pwrite not implemented\n"); 
    abort(); 
    return -1; 
}

ssize_t __send_abort(void* context, int socket, const void *buffer, size_t length, int flags) { 
    fprintf(stderr, "_send_abort: send not implemented\n"); 
    abort(); 
    return -1; 
}


ssize_t __sendmsg_abort(void* context, int socket, const struct msghdr *message, int flags) { 
    fprintf(stderr, "_sendmsg_abort: sendmsg not implemented\n"); 
    abort(); 
    return -1; 
}

ssize_t __sendto_abort(void* context, int socket, const void *buffer, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) { 
    fprintf(stderr, "_sendto_abort: sendto not implemented\n"); 
    abort(); 
    return -1; 
}

ssize_t __recv_abort(void* ctxt, int socket, void *buffer, size_t length, int flags){ 
    fprintf(stderr, "__recv_abort: sendto not implemented\n"); 
    abort(); 
    return -1; 
}

ssize_t __recvfrom_abort(void* ctxt, int socket, void *restrict buffer, size_t length, int flags, struct sockaddr *restrict address, socklen_t *restrict address_len){ 
    fprintf(stderr, "__recvfrom_abort: sendto not implemented\n"); 
    abort(); 
    return -1; 
}

ssize_t __recvmsg_abort(void* ctxt, int socket, struct msghdr *message, int flags){ 
    fprintf(stderr, "__recvmsg_abort: sendto not implemented\n"); 
    abort(); 
    return -1; 
}

int __close_abort(void* context, int fildes) {
    fprintf(stderr, "__close_abort: abort not implemented\n"); 
    abort(); 
    return -1; 
}
     
// abort functions 

static intercept_t abort_interceptor = {.accept = __accept_abort, 
 .read = __read_abort, .write = __write_abort, .pread = __pread_abort,
 .pwrite = __pwrite_abort, //.send = __send_abort, 
 .sendmsg = __sendmsg_abort, .sendto = __sendto_abort, 
 //.recv = __recv_abort, 
 .recvfrom = __recvfrom_abort, .recvmsg = __recvmsg_abort,
 .close = __close_abort };

 
// we support MAX_FDS_SUPPORTED file descriptors - define appropriately

static intercept_t *interceptor[MAX_FDS_SUPPORTED] = { 0 } ;
static void* context[MAX_FDS_SUPPORTED] = { 0 } ;


intercept_t* get_interceptor(int fd) {
    if (fd < 0 || fd >= MAX_FDS_SUPPORTED) {
        return &abort_interceptor;
    }
    return interceptor[fd];
}

void* get_context(int fd) {
    if (fd < 0 || fd >= MAX_FDS_SUPPORTED) {
        return NULL;
    }
    return context[fd];
}


intercept_t* set_interceptor(int fd, intercept_t* new_i, void* ctxt) {
    if (fd < 0 || fd >= MAX_FDS_SUPPORTED) {
        return NULL;
    }
    interceptor[fd] = new_i; 
    context[fd] = ctxt;
    return new_i;
}

void  remover_interceptor(int fd) {
    set_interceptor(fd, NULL, NULL);
}

/*
functions creating file descriptors ( https://en.wikipedia.org/wiki/File_descriptor#Creating_file_descriptors )

We need to overload these functions in case we want to wrap, for example, 
a stream into a TLS stream.  

The following functions can create file descriptors:

open()
creat()
socket()
accept()
socketpair()
pipe()
opendir()


If we need to replace the implementation of one of these function, we just define a strong symbol
that will replace the weak symbol.
*/


/*
https://en.wikipedia.org/wiki/File_descriptor#Operations_on_a_single_file_descriptor
*/

ssize_t WRITE_WRAPPER(int fd, const void *buf, size_t nbyte) {
    intercept_t* it = get_interceptor(fd);
    if (!it) {
        return _write(fd, buf, nbyte);
    }

    return it->write(get_context(fd), fd, buf, nbyte);
}

int ACCEPT_WRAPPER(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len) {
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _accept(socket, address, address_len);
    }
    return it->accept4(get_context(socket), socket, address, address_len, 0);
}

int ACCEPT4_WRAPPER(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len, int flags) {
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _accept4(socket, address, address_len, flags);
    }
    return it->accept4(get_context(socket), socket, address, address_len, flags);
}

int BIND_WRAPPER(int socket, const struct sockaddr* address, socklen_t address_len) {
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _bind(socket, address, address_len);
    }
    return it->bind(get_context(socket), socket, address, address_len);
}

int CONNECT_WRAPPER(int socket, const struct sockaddr* address, socklen_t address_len) {
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _connect(socket, address, address_len);
    }
    return it->connect(get_context(socket), socket, address, address_len);
}

ssize_t RECV_WRAPPER(int socket, void *buffer, size_t length, int flags) {
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _recv(socket, buffer, length, flags);
    }
    return it->recv(get_context(socket), socket, buffer, length, flags);
}

ssize_t RECVFROM_WRAPPER(int socket, void *restrict buffer, size_t length, int flags, struct sockaddr *restrict address, socklen_t *restrict address_len) {
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _recvfrom(socket, buffer, length, flags, address, address_len);
    }
    return it->recvfrom(get_context(socket), socket, buffer, length, flags, address, address_len);
}

ssize_t RECVMSG_WRAPPER(int socket, struct msghdr *message, int flags){ 
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _recvmsg(socket, message, flags);
    }
    return it->recvmsg(get_context(socket), socket, message, flags);
}

int CLOSE_WRAPPER(int fildes) {
    intercept_t* it = get_interceptor(fildes);
    if (!it) {
        return _close(fildes);
    }
    return it->close(get_context(fildes), fildes);
}

ssize_t READ_WRAPPER(int a1, void *a2, size_t a3) { 
    intercept_t* it = get_interceptor(a1);
    if (!it) {
        return _read(a1, a2, a3);
    }
    return it->read(get_context(a1), a1, a2, a3);
}

ssize_t PREAD_WRAPPER(int a1, void *a2, size_t a3, off_t a4) {
    intercept_t* it = get_interceptor(a1);
    if (!it) {
        return _pread(a1, a2, a3, a4);
    }
    return it->pread(get_context(a1), a1, a2, a3, a4);
}

ssize_t PWRITE_WRAPPER(int a1, const void *a2, size_t a3, off_t a4) {
    intercept_t* it = get_interceptor(a1);
    if (!it) {
        return _pwrite(a1, a2, a3, a4);
    }
    return it->pwrite(get_context(a1), a1, a2, a3, a4);
}

ssize_t SEND_WRAPPER(int socket, const void *buffer, size_t length, int flags) {
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _send(socket, buffer, length, flags);
    }
    return it->send(get_context(socket), socket, buffer, length, flags);
}

ssize_t SENDMSG_WRAPPER(int socket, const struct msghdr *message, int flags) {
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _sendmsg(socket, message, flags);
    }
    return it->sendmsg(get_context(socket), socket, message, flags);
}

ssize_t SENDTO_WRAPPER(int socket, const void *buffer, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len){
    intercept_t* it = get_interceptor(socket);
    if (!it) {
        return _sendto(socket, buffer, length, flags, dest_addr, dest_len);
    }
    return it->sendto(get_context(socket), socket, buffer, length, flags, dest_addr, dest_len);
}

ssize_t WRITEV_WRAPPER(int fd, const struct iovec *iov, int iovcnt) {
    intercept_t* it = get_interceptor(fd);
    if (!it) {
        return _writev(fd, iov, iovcnt);
    }
    return it->writev(get_context(fd), fd, iov, iovcnt);
}

#endif

