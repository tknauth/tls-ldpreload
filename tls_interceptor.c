/*
  libc interceptor:
    
  objective: if no interceptor activated (during compilation), we have no overhead
    
  if interceptor is activated: we introduce some overhead since we wrap some libc functions

  todo: replace err_sys by logging function
*/

#ifdef TLS_INTERCEPTOR

#include <stdio.h>
#include <wolfssl/options.h>
#include <wolfssl/ssl.h>
#include <wolfssl/test.h>
#include <errno.h>
#include <sys/stat.h>

#include "fdfunctions_musl.h"
#include "interceptor.h"

#if 0
int init_tls_interceptor() {
    /* Initialize wolfSSL library */
    wolfSSL_Init();
    return 1;
}
#endif

   
int accept_tls(void* ctxt, int sockfd, struct sockaddr *restrict address, socklen_t *restrict address_len);
int accept4_tls(void* ctxt, int sockfd, struct sockaddr *restrict address, socklen_t *restrict address_len, int flags);
int bind_tls(void* ctxt, int sockfd, const struct sockaddr* addr, socklen_t addrlen);
int connect_tls(void* ctxt, int socket, const struct sockaddr* address, socklen_t address_len);
int close_tls(void* ctxt, int fildes);
ssize_t write_tls(void* ctxt, int fd, const void *buf, size_t nbyte);
ssize_t read_tls(void* ctxt, int fd, void *buf, size_t nbyte);
ssize_t send_tls(void* ctxt, int socket, const void *buffer, size_t length, int flags);
ssize_t recv_tls(void* ctxt, int socket, void *buffer, size_t length, int flags);
ssize_t recvfrom_tls(void* ctxt, int socket, void *restrict buffer, size_t length, int flags, struct sockaddr *restrict address, socklen_t *restrict address_len);
ssize_t recvmsg_tls(void* ctxt, int socket, struct msghdr *message, int flags);
ssize_t sendmsg_tls(void* ctxt, int sockfd, const struct msghdr *msg, int flags);
ssize_t writev_tls(void* ctxt, int fd, const struct iovec *iov, int iovcnt);

intercept_t tls_interceptor = {
    .accept = accept_tls,
    .accept4 = accept4_tls,
    .bind = bind_tls,
    .connect = connect_tls,
    .read = read_tls, .write = write_tls,
    .pread = __pread_abort, .pwrite = __pwrite_abort,
    .send = send_tls,
    .sendmsg = sendmsg_tls,
    .sendto = __sendto_abort,
    .recv = recv_tls,
    .recvfrom = recvfrom_tls, .recvmsg = recvmsg_tls,
    .close = close_tls,
    .writev = writev_tls
};

/* 
   to keep some flexibility - lets use some extra struct instead of using a pointer to a wolfssl struct
*/

typedef struct context {
    WOLFSSL_CTX* ctx;
    WOLFSSL* ssl;
} TLS_CONTEXT;

static TLS_CONTEXT* TLS_CONTEXT_new() {
    TLS_CONTEXT* ctxt = malloc(sizeof(TLS_CONTEXT));
    if (ctxt == NULL) {
        err_sys("TLS_CONTEXT_new: failed to allocated context");
    } else {
        ctxt->ctx = NULL;
        ctxt->ssl = NULL;
    }
    return ctxt;
}

static void TLS_CONTEXT_free(TLS_CONTEXT* ctxt) {
    if (ctxt == NULL) {
        err_sys("TLS_CONTEXT_free: context is NULL");
        return;        
    }
    if (ctxt->ssl != NULL) {
        wolfSSL_free(ctxt->ssl);
        ctxt->ssl = NULL;
    }
    if (ctxt->ctx != NULL) {
        wolfSSL_CTX_free(ctxt->ctx);
        ctxt->ctx = NULL;
    }
    free(ctxt);
}

void load_certificate_and_key(WOLFSSL_CTX* ctx) {
    /* todo: replace by load from encrypted config file  - or ensure that filesystem is encrypted  */
    struct stat sbuf;
    if (stat("./cert.pem", &sbuf) < 0) {
        err_sys("socket_tls: Error reading cert.pem");
        abort();
    }

    FILE* f = fopen("./cert.pem", "r");
    void* certificate = malloc(sbuf.st_size);
    fread(certificate, sbuf.st_size, 1, f);
    fclose(f);

    if (wolfSSL_CTX_use_certificate_buffer(ctx, certificate, sbuf.st_size,
                                           SSL_FILETYPE_PEM) != SSL_SUCCESS) {
        err_sys("socket_tls: Error loading certs/server-cert.pem");
        abort();
		}

    if (stat("./key.pem", &sbuf) < 0) {
        err_sys("socket_tls: Error reading key.pem");
        abort();
    }
    
    f = fopen("./key.pem", "r");
    void* private_key = malloc(sbuf.st_size);
    fread(private_key, sbuf.st_size, 1, f);
    fclose(f);

    /* todo: replace by load from encrypted config file - or ensure that filesystem is encrypted */
    if (wolfSSL_CTX_use_PrivateKey_buffer(ctx, private_key, sbuf.st_size,
                                          SSL_FILETYPE_PEM) != SSL_SUCCESS) {
        err_sys("socket_tls: Error loading certs/server-key.pem");
        abort();
    }
}

// overload original socket function

int SOCKET_WRAPPER(int domain, int type, int protocol) {
    if (domain != PF_INET && domain != PF_INET6) {
        errno = EAFNOSUPPORT;
        return -1;
    }
    
    if (!(type & SOCK_STREAM)) { /* must use & here as type also carries flags */
        return _socket(domain, type, protocol);
    }

    /* Thomas: We cannot distinguish between server and client
       here. Server must be initialized in bind(), listen() or accept(). Client
       must be initialized in connect(). */
    TLS_CONTEXT* nc = TLS_CONTEXT_new();

    if (nc == NULL) {
        err_sys("socket_tls: cannot allocate context");
        errno = ENOMEM;
        return -1;
    }

    int sfd = _socket(domain, type, protocol);
    if (sfd >= 0) {
        set_interceptor(sfd, &tls_interceptor, nc);  
        return sfd;
    }

error:
    if (nc != NULL) TLS_CONTEXT_free(nc);
    return -1;
}

int accept_tls(void* ctxt, int sockfd, struct sockaddr *restrict address, socklen_t *restrict address_len) {
    return accept4_tls(ctxt, sockfd, address, address_len, 0);
}

int accept4_tls(void* ctxt, int sockfd, struct sockaddr *restrict address, socklen_t *restrict address_len, int flags) {
    WOLFSSL_CTX* ctx = ((TLS_CONTEXT*) ctxt)->ctx;
    WOLFSSL* ssl;    

    if (ctx == NULL) {
        err_sys("accept_tls: ctx undefined error");
        return -1;
    }

    TLS_CONTEXT* nc = TLS_CONTEXT_new();
    if (nc == NULL) {
        err_sys("socket_tls: cannot allocate context");
        errno = ENOMEM;
        return -1;
    }
    
    int clientfd = _accept(sockfd, address, address_len);


    if (WOLFSSL_SOCKET_IS_INVALID(clientfd)) {
        goto error;
    }

    if ( (ssl = wolfSSL_new(ctx)) == NULL) {
        err_sys("accept_tls: wolfSSL_new error");
        goto error;
    }
    wolfSSL_set_fd(ssl, clientfd);
    nc->ssl = ssl;
    set_interceptor(clientfd, &tls_interceptor, nc);   
    return clientfd;

 error:
    if (nc != NULL) TLS_CONTEXT_free(nc);
    return -1;

}

int bind_tls(void* ctxt, int sockfd, const struct sockaddr* addr, socklen_t addrlen) {
    
    WOLFSSL_METHOD* method = wolfTLSv1_2_server_method(); /* todo: switch to v3  */

    WOLFSSL_CTX* srv_ctx = wolfSSL_CTX_new(method);
    if (srv_ctx == NULL) {
        err_sys("socket_tls: wolfSSL_CTX_new error");
        abort();
    }

    load_certificate_and_key(srv_ctx);

    ((TLS_CONTEXT*) ctxt)->ctx = srv_ctx;

    int ret = _bind(sockfd, addr, addrlen);
    return ret;
}

int connect_tls(void* ctxt, int sockfd, const struct sockaddr* addr, socklen_t addrlen) {

    WOLFSSL_METHOD* method = wolfTLSv1_2_client_method(); /* todo: switch to v3  */
    WOLFSSL_CTX* wolfssl_ctx = wolfSSL_CTX_new(method);
    
    if (wolfSSL_CTX_load_verify_locations(wolfssl_ctx, "./cert.pem", 0) !=
           SSL_SUCCESS) {
       fprintf(stderr, "Error loading cert.pem please check the file.\n");
       exit(EXIT_FAILURE);
    }

    WOLFSSL* ssl = wolfSSL_new(wolfssl_ctx);
    if (ssl == NULL) {
        err_sys("connect_tls: wolfSSL_new error");
        abort();
    }
    
    ((TLS_CONTEXT*) ctxt)->ctx = wolfssl_ctx;
    ((TLS_CONTEXT*) ctxt)->ssl = ssl;

    int connect_ret = _connect(sockfd, addr, addrlen);
    //assert(ret == 0);
    
    int ret = wolfSSL_set_fd(ssl, sockfd);
    assert(ret == SSL_SUCCESS);
    
    /* The TLS handshake is implicitly performed on wolfSSL_write if
       it hasn't happend already. */
    
    /* ret = wolfSSL_connect(ssl); */
    /* assert(ret == SSL_SUCCESS); */

    return connect_ret;
}

ssize_t write_tls(void* ctxt, int fd, const void *buf, size_t nbyte) {
    WOLFSSL* ssl = ((TLS_CONTEXT*) ctxt)->ssl;
    if ( ssl == NULL) {
        err_sys("write_tls: context not defined");
        return -1;
    }
    return wolfSSL_write(ssl, buf, nbyte);
}


ssize_t read_tls(void* ctxt, int fd, void *buf, size_t nbyte) {
    WOLFSSL* ssl = ((TLS_CONTEXT*) ctxt)->ssl;
    if ( ssl == NULL) {
        err_sys("read_tls: context not defined");
        return -1;
    }
    return wolfSSL_read(ssl, buf, nbyte);
}

// pread - does not make sense on a socket
//  - just abort for now - if needed, we will add

// pwrite - does not make sense on a socket
//  - just abort for now - if needed, we will add


ssize_t send_tls(void* ctxt, int socket, const void *buffer, size_t length, int flags) {
    WOLFSSL* ssl = ((TLS_CONTEXT*) ctxt)->ssl;
    if ( ssl == NULL) {
        err_sys("send_tls: context not defined");
        return -1;
    }
    return wolfSSL_send(ssl, buffer, length, flags);    
}

ssize_t sendmsg_tls(void* ctxt, int socket, const struct msghdr *message, int flags) {
    WOLFSSL* ssl = ((TLS_CONTEXT*) ctxt)->ssl;
    if ( ssl == NULL) {
        err_sys("send_tls: context not defined");
        return -1;
    }
    if (message->msg_name != 0) {
        err_sys("sendmsg_tls: msg_name, not supported");
        return -1;
    }

    return wolfSSL_writev(ssl, message->msg_iov, message->msg_iovlen);
}

// todo: sendto_tls - abort for now
/*
  ssize_t sendto_tls(void* ctxt, int socket, const void *buffer, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len) {
  WOLFSSL* ssl = ((TLS_CONTEXT*) ctxt)->ssl;
  if ( ssl == NULL) {
  err_sys("send_tls: context not defined");
  return -1;
  }
  return wolfSSL_sendto(ssl, buffer, length, flags, dest_addr, dest_len);    
  }
*/


ssize_t recv_tls(void* ctxt, int socket, void *buffer, size_t length, int flags) { 
    WOLFSSL* ssl = ((TLS_CONTEXT*) ctxt)->ssl;
    if ( ssl == NULL) {
        err_sys("send_tls: context not defined");
        return -1;
    }
    return wolfSSL_recv(ssl, buffer, length, flags); 
}

// todo: implement
ssize_t recvfrom_tls(void* ctxt, int socket, void *restrict buffer, size_t length, int flags, struct sockaddr *restrict address, socklen_t *restrict address_len){ 
    if (address != 0) {
        err_sys("recvfrom: not implemented \n");
        return -1;
    }
    WOLFSSL* ssl = ((TLS_CONTEXT*) ctxt)->ssl;
    if ( ssl == NULL) {
        err_sys("send_tls: context not defined");
        return -1;
    }
    return wolfSSL_recv(ssl, buffer, length, flags); 
}


// todo: implement
ssize_t recvmsg_tls(void* ctxt, int socket, struct msghdr *message, int flags){ 
    fprintf(stderr, "recvmsg_tls: sendto not implemented\n"); 
    abort(); 
    
    return -1; 
}


// note: we assuem that if a file descriptor is closed, no more parallel operations are performed on this file descriptor!
//  otherwise, an application might directly access the file descriptor without operations

int close_tls(void* ctxt, int fildes) {
    TLS_CONTEXT_free((TLS_CONTEXT*) ctxt);
    int ret =  _close(fildes);
    remover_interceptor(fildes);
    return  ret;
}

ssize_t writev_tls(void* ctxt, int fd, const struct iovec *iov, int iovcnt) {
    WOLFSSL* ssl = ((TLS_CONTEXT*) ctxt)->ssl;
    return wolfSSL_writev(ssl, iov, iovcnt);
}

/*

  wolfSSL configuration

  #define NO_WOLFSSL_CLIENT
  #define NO_DES3
  NO_DH
  NO_MD4
  NO_MD5
  NO_RC4
  NO_RABBIT
  NO_HC128
  WOLFSSL_DTLS
  WOLFSSL_SHA384
  WOLFSSL_SHA512
  HAVE_AESGCM
  HAVE_ECC
  NO_FILESYSTEM
  NO_DEV_RANDOM
  USER_TICKS
  WOLFSSL_AESNI

*/

#endif
