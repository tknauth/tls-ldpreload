#ifndef __INTERCEPTOR_H__
#define __INTERCEPTOR_H__


/*
    interceptor struct

    todo: add all fd-related functions  - see https://en.wikipedia.org/wiki/File_descriptor#Operations_on_a_single_file_descriptor
*/


typedef struct {
    int (*accept)(void* context, int socket, struct sockaddr *restrict address, socklen_t *restrict address_len);
    int (*accept4)(void* context, int socket, struct sockaddr *restrict address, socklen_t *restrict address_len, int flags);
    int (*bind)(void* context, int socket, const struct sockaddr* address, socklen_t address_len);
    int (*connect)(void* context, int socket, const struct sockaddr* address, socklen_t address_len);
    ssize_t (*read)(void* context, int, void *, size_t);
    ssize_t (*write)(void* context, int, const void *, size_t);
    ssize_t (*pread)(void* context, int, void *, size_t, off_t);
    ssize_t (*pwrite)(void* context, int, const void *, size_t, off_t);
    ssize_t (*send)(void* context, int socket, const void *buffer, size_t length, int flags);
    ssize_t (*sendmsg)(void* context, int socket, const struct msghdr *message, int flags);
    ssize_t (*sendto)(void* context, int socket, const void *buffer, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
    ssize_t (*recv)(void* ctxt, int socket, void *buffer, size_t length, int flags);
    ssize_t (*recvfrom)(void* ctxt, int socket, void *restrict buffer, size_t length, int flags, struct sockaddr *restrict address, socklen_t *restrict address_len);
    ssize_t (*recvmsg)(void* ctxt, int socket, struct msghdr *message, int flags);
    int (*close)(void* context, int fildes);
    ssize_t (*writev)(void* ctxt, int fd, const struct iovec *iov, int iovcnt);

// todo: add more ...

} intercept_t;

intercept_t* get_interceptor(int fd);
void* get_context(int fd);
intercept_t* set_interceptor(int fd, intercept_t* new_i, void* ctxt);
void  remover_interceptor(int fd);
    

ssize_t __write_abort(void* context, int fildes, const void *buf, size_t nbyte);
int __accept_abort(void* context, int socket, struct sockaddr *restrict address, socklen_t *restrict address_len);
ssize_t __read_abort(void* context, int a1, void *a2, size_t a3);
ssize_t __pread_abort(void* context, int a1, void *a2, size_t a3, off_t a4);
ssize_t __pwrite_abort(void* context, int a1, const void *a2, size_t a3, off_t a4);
ssize_t __send_abort(void* context, int socket, const void *buffer, size_t length, int flags);
ssize_t __sendmsg_abort(void* context, int socket, const struct msghdr *message, int flags);
ssize_t __sendto_abort(void* context, int socket, const void *buffer, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
ssize_t __recv_abort(void* ctxt, int socket, void *buffer, size_t length, int flags);
ssize_t __recvfrom_abort(void* ctxt, int socket, void *restrict buffer, size_t length, int flags, struct sockaddr *restrict address, socklen_t *restrict address_len);
ssize_t __recvmsg_abort(void* ctxt, int socket, struct msghdr *message, int flags);

int __close_abort(void* context, int fildes);

#define WRITE_WRAPPER write
#define SOCKET_WRAPPER socket
#define ACCEPT_WRAPPER accept
#define ACCEPT4_WRAPPER accept4
#define BIND_WRAPPER  bind
#define CONNECT_WRAPPER connect
#define READ_WRAPPER read
#define PREAD_WRAPPER pread
#define PWRITE_WRAPPER pwrite
#define SEND_WRAPPER send
#define SENDMSG_WRAPPER sendmsg
#define SENDTO_WRAPPER sendto
#define RECV_WRAPPER recv
#define RECVMSG_WRAPPER recvmsg
#define RECVFROM_WRAPPER recvfrom
#define CLOSE_WRAPPER close
#define STDIO_READ_WRAPPER __stdio_read
#define STDIO_WRITE_WRAPPER __stdio_write
#define WRITEV_WRAPPER writev

#endif 
