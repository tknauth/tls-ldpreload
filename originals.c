#ifdef TLS_INTERCEPTOR
#define _GNU_SOURCE

#include <sys/socket.h>
#include <errno.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

#include "fdfunctions_musl.h"
#include "interceptor.h"

#ifndef MAX_FDS_SUPPORTED
#define MAX_FDS_SUPPORTED 1024
#endif

typedef struct {
    int (*accept)(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len);
    int (*accept4)(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len, int flags);
    int (*bind)(int socket, const struct sockaddr* address, socklen_t address_len);
    int (*connect)(int socket, const struct sockaddr* address, socklen_t address_len);
    ssize_t (*read)(int, void *, size_t);
    ssize_t (*write)(int, const void *, size_t);
    ssize_t (*pread)(int, void *, size_t, off_t);
    ssize_t (*pwrite)(int, const void *, size_t, off_t);
    ssize_t (*send)(int socket, const void *buffer, size_t length, int flags);
    ssize_t (*sendmsg)(int socket, const struct msghdr *message, int flags);
    ssize_t (*sendto)(int socket, const void *buffer, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len);
    ssize_t (*recv)(int socket, void *buffer, size_t length, int flags);
    ssize_t (*recvfrom)(int socket, void *restrict buffer, size_t length, int flags, struct sockaddr *restrict address, socklen_t *restrict address_len);
    ssize_t (*recvmsg)(int socket, struct msghdr *message, int flags);
    int (*close)(int fildes);
    ssize_t (*writev)(int fd, const struct iovec *iov, int iovcnt);
    int (*socket)(int domain, int type, int protocol);

} fcnts_t;

static fcnts_t fs;

ssize_t _write(int fd, const void *buf, size_t nbyte) {
    if (fs.write == NULL) {
        fs.write = dlsym(RTLD_NEXT, "write");
    }
    return fs.write(fd, buf, nbyte);
}

int _accept(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len) {
    if (fs.accept == NULL) {
        fs.accept = dlsym(RTLD_NEXT, "accept");
    }
    return fs.accept(socket, address, address_len);
}

int _accept4(int socket, struct sockaddr *restrict address, socklen_t *restrict address_len, int flags) {
    if (fs.accept4 == NULL) {
        fs.accept4 = dlsym(RTLD_NEXT, "accept4");
    }
    return fs.accept4(socket, address, address_len, flags);
}

int _bind(int socket, const struct sockaddr* address, socklen_t address_len) {
    if (fs.bind == NULL) {
        fs.bind = dlsym(RTLD_NEXT, "bind");
    }
    return fs.bind(socket, address, address_len);
}

int _connect(int socket, const struct sockaddr* address, socklen_t address_len) {
    if (fs.connect == NULL) {
        fs.connect = dlsym(RTLD_NEXT, "connect");
    }
    return fs.connect(socket, address, address_len);
}

ssize_t _recv(int socket, void *buffer, size_t length, int flags) { 
    if (fs.recv == NULL) {
        fs.recv = dlsym(RTLD_NEXT, "recv");
    }
    return fs.recv(socket, buffer, length, flags);
}


ssize_t _recvfrom(int socket, void *restrict buffer, size_t length, int flags, struct sockaddr *restrict address, socklen_t *restrict address_len) {
    if (fs.recvfrom == NULL) {
        fs.recvfrom = dlsym(RTLD_NEXT, "recvfrom");
    }
    return fs.recvfrom(socket, buffer, length, flags, address, address_len);
}


ssize_t _recvmsg(int socket, struct msghdr *message, int flags){ 
    if (fs.recvmsg == NULL) {
        fs.recvmsg = dlsym(RTLD_NEXT, "recvmsg");
    }
    return fs.recvmsg(socket, message, flags);
}

int _close(int fd) {
    if (fs.close == NULL) {
        fs.close = dlsym(RTLD_NEXT, "close");
    }
    return fs.close(fd);
}

ssize_t _read(int a1, void *a2, size_t a3) { 
    if (fs.read == NULL) {
        fs.read = dlsym(RTLD_NEXT, "read");
    }
    return fs.read(a1, a2, a3);
}

ssize_t _pread(int a1, void *a2, size_t a3, off_t a4) {
    if (fs.pread == NULL) {
        fs.pread = dlsym(RTLD_NEXT, "pread");
    }
    return fs.pread(a1, a2, a3, a4);
}

ssize_t _pwrite(int a1, const void *a2, size_t a3, off_t a4) {
    if (fs.pwrite == NULL) {
        fs.pwrite = dlsym(RTLD_NEXT, "pwrite");
    }
    return fs.pwrite(a1, a2, a3, a4);
}

ssize_t _send(int socket, const void *buffer, size_t length, int flags) {
    if (fs.send == NULL) {
        fs.send = dlsym(RTLD_NEXT, "send");
    }
    return fs.send(socket, buffer, length, flags);
}

ssize_t _sendmsg(int socket, const struct msghdr *message, int flags) {
    if (fs.sendmsg == NULL) {
        fs.sendmsg = dlsym(RTLD_NEXT, "sendmsg");
    }
    return fs.sendmsg(socket, message, flags);
}

ssize_t _sendto(int socket, const void *buffer, size_t length, int flags, const struct sockaddr *dest_addr, socklen_t dest_len){
    if (fs.sendto == NULL) {
        fs.sendto = dlsym(RTLD_NEXT, "sendto");
    }
    return fs.sendto(socket, buffer, length, flags, dest_addr, dest_len);
}

int _socket(int domain, int type, int protocol) {
    if (fs.socket == NULL) {
        fs.socket = dlsym(RTLD_NEXT, "socket");
    }
    return fs.socket(domain, type, protocol);
}

ssize_t _writev(int fd, const struct iovec *iov, int iovcnt) {
    if (fs.writev == NULL) {
        fs.writev = dlsym(RTLD_NEXT, "writev");
    }
    return fs.writev(fd, iov, iovcnt);
}

#endif

