CFLAGS=-DTLS_INTERCEPTOR -Ideps/include -g
LDFLAGS=deps/lib/libwolfssl.a -ldl

tls-preload.so : interceptor.c tls_interceptor.c originals.c
	$(CC) -shared -fPIC $(CFLAGS) $^ -o $@ $(LDFLAGS)
