#!/bin/bash

[[ ! -e wolfssl ]] && git clone https://github.com/wolfSSL/wolfssl.git

pushd wolfssl
git checkout 3da5ddd49e216b2396324c420b9001f9e8855991
patch -p1 -N <<EOF
diff --git a/wolfssl/io.h b/wolfssl/io.h
index c9d859b..0e803d6 100644
--- a/wolfssl/io.h
+++ b/wolfssl/io.h
@@ -238,6 +238,12 @@
 #elif defined(WOLFSSL_VXWORKS)
     #define SEND_FUNCTION send
     #define RECV_FUNCTION recv
+#elif defined(LD_PRELOAD)
+ssize_t _recv(int socket, void *buffer, size_t length, int flags);
+ssize_t _send(int socket, const void *buffer, size_t length, int flags);
+
+    #define SEND_FUNCTION _send
+    #define RECV_FUNCTION _recv
 #else
     #define SEND_FUNCTION send
     #define RECV_FUNCTION recv
EOF

[[ ! -e configure ]] && ./autogen.sh

CFLAGS="-DLD_PRELOAD" LIBS="-ldl" ./configure --with-pic \
      --disable-dynamic --enable-static \
      --enable-aesni \
      --prefix=`readlink -f ../deps` \
      --disable-examples --disable-crypttests # Building binaries will fail due to missing symbols.

make clean
make
make install
popd

make
